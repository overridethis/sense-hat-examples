import morse_talk as mtalk
import sys
import time
from sense_hat import SenseHat

sos = mtalk.encode('SOS')
g = (0,255,0)
_ = (0,0,0)

dot = [
  _, _, _, _, _, _, _, _,
  _, _, _, g, g, _, _, _,
  _, _, g, g, g, g, _, _,
  _, g, g, g, g, g, g, _,
  _, g, g, g, g, g, g, _,
  _, _, g, g, g, g, _, _,
  _, _, _, g, g, _, _, _,
  _, _, _, _, _, _, _, _,
]


dash = [
  _, _, _, _, _, _, _, _,
  _, _, _, _, _, _, _, _,
  _, _, _, _, _, _, _, _,
  _, g, g, g, g, g, g, _,
  _, g, g, g, g, g, g, _,
  _, _, _, _, _, _, _, _,
  _, _, _, _, _, _, _, _,
  _, _, _, _, _, _, _, _,
]

line_break = '\n'

hat = SenseHat()

sys.stdout.write("[Enter to Quit]\n")
sys.stdout.write("Text to transmit: ")
to_transmit = sys.stdin.readline()

# while there is something to encode to morse.
while to_transmit != line_break:

    # encode to morse.
    morse = mtalk.encode(to_transmit)
    print('MORSE: ' + morse)

    # present message three times
    for x in range(0,3):

        # loop through characters
        for action in morse:

            # output char to led.
            if action in ['-','.']: 
                hat.set_pixels(dash if action == '-' else dot)
                time.sleep(0.30 if action == '-' else 0.10)
                hat.clear()
                time.sleep(0.10)
            else:
                time.sleep(0.10)

            # general wait and clear.
            hat.clear()
        
    # query for new data.
    sys.stdout.write("[Enter to Quit]\n")
    sys.stdout.write("Text to transmit: ")
    to_transmit = sys.stdin.readline()

hat.clear()
