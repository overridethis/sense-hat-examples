from sense_hat import SenseHat
import time

r = (255,255,0)
hat = SenseHat()

for i in range(0,4):
    c = hat.get_temperature()
    f = 9.0/5.0 * c + 32
    hat.show_message('Temperature %d F' % f, 0.05, r)
    time.sleep(0.5)
