import twitter
import time
from sense_hat import SenseHat

_ = [0,0,0]
b = [0,0,255]
w = [255,255,255]
twitter_logo = [
        _,_,b,b,b,b,_,_,
        _,b,b,b,w,b,b,_,
        b,b,b,w,w,w,w,b,
        b,w,b,b,w,w,b,b,
        b,w,w,w,w,w,w,b,
        b,b,w,w,w,w,b,b,
        _,b,b,w,w,b,b,_,
        _,_,b,b,b,b,_,_,
    ]

# create your own application at https://developer.twitter.com.
api = twitter.Api(consumer_key='{YOUR_CONSUMER_KEY}',
    consumer_secret='{YOUR_CONSUMER_SECRET}',
    access_token_key='{YOUR_ACCESS_TOKEN_KEY}',
    access_token_secret='{YOUR_ACCESS_TOKEN_SECRET}')

def flash_logo():
    for i in range(0,3):
        hat.set_pixels(twitter_logo)        
        time.sleep(0.5)
        hat.clear()
        time.sleep(0.5)

timeline = api.GetMentions(1)
current = timeline.pop()  
id = 1 ## just some random number

hat = SenseHat()
hat.clear()

while True:
    
    if (id != current.id):
        msg = '[{}] {}'.format(current.id, current.text.encode('utf-8','ignore'))
        print msg
        flash_logo()
        hat.show_message(msg)
        id = current.id

    time.sleep(60)
    
    current = api.GetMentions(1).pop()
