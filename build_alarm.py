from datetime import datetime
import requests
from sense_hat import SenseHat
from time import sleep

_ = [0,0,0]
r = [255,0,0]
g = [1,50,32]
b = [0,0,255]
w = [150,150,150]
y = [200,200,0]


running_one = [
    _,_,y,y,y,y,_,_,
    _,y,y,y,y,y,y,_,
    y,y,_,_,_,_,y,y,
    y,y,y,_,_,y,y,y,
    y,y,y,_,_,y,y,y,
    y,y,_,_,_,_,y,y,
    _,y,y,y,y,y,y,_,
    _,_,y,y,y,y,_,_,
]

running_two = [
    _,_,y,y,y,y,_,_,
    _,y,y,y,y,y,y,_,
    y,y,_,y,y,_,y,y,
    y,y,_,_,_,_,y,y,
    y,y,_,_,_,_,y,y,
    y,y,_,y,y,_,y,y,
    _,y,y,y,y,y,y,_,
    _,_,y,y,y,y,_,_,
]

success = [
    _,_,g,g,g,g,_,_,
    _,g,w,g,g,w,g,_,
    g,g,w,g,g,w,g,g,
    g,g,g,g,g,g,g,g,
    g,w,g,g,g,g,w,g,
    g,g,w,g,g,w,g,g,
    _,g,g,w,w,g,g,_,
    _,_,g,g,g,g,_,_,
]

failed = [
    _,_,r,r,r,r,_,_,
    _,r,r,y,y,r,r,_,
    r,r,r,y,y,r,r,r,
    r,r,r,y,y,r,r,r,
    r,r,r,y,y,r,r,r,
    r,r,r,r,r,r,r,r,
    _,r,r,y,y,r,r,_,
    _,_,r,r,r,r,_,_,
]

def get_build_status():
    r = requests \
        .get('https://ci.appveyor.com/api/projects/{username}/{project}') \
        .json()
        
    return r["build"]["status"]

def do_running_animation():
    for i in range(5):
        hat.set_pixels(running_one)
        sleep(1)
        hat.set_pixels(running_two)
        sleep(1)
        

hat = SenseHat()
while True:
    status = get_build_status()
    print '[{}] status: {}'.format(datetime.utcnow(), status)

    if status == 'success':
        hat.set_pixels(success)
        sleep(5)
    elif status == 'failed':
        hat.set_pixels(failed)
        sleep(5)
    else: 
        do_running_animation()

    



