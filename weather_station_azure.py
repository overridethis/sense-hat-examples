import requests
import uuid
from datetime import datetime
from time import sleep
from sense_hat import SenseHat
from pyfiglet import Figlet

from azure.servicebus import ServiceBusService

_ = (0,0,0)
g = (0,255,0)
b = (0,0,255)
f = Figlet(font='3x5')

def display_progress_bar():
    sleep(5)
    for i in range(0,7):
        hat.set_pixel(i,7,b)
        sleep(5)

def display_on_lcd(temp):
    formatted = f.renderText('{:.0f}'.format(temp))
    chars = filter(lambda c: c <> '\n', list(formatted))
    pixels = map(lambda c: g if c == '#' else _, chars)
    pixels.extend([_] * 16) 
    hat.set_pixels(pixels)

def convert_to_F(temp):
    return temp * 9 / 5 + 32

def post_to_azure(temp, hum):
    sbs = ServiceBusService(
        service_namespace='{YOUR_SERVICE_NAMESPACE}',
        shared_access_key_name='{YOUR_SHARED_ACCESS_KEY_NAME}',
        shared_access_key_value='{YOUR_SHARED_ACCESS_KEY_VALUE}')
    payload = { 'temperature': temp, 'humidity': hum, 'date': datetime.utcnow().isoformat(), 'id': str(uuid.uuid4()) }
    sbs.send_event('weatherstation', payload)

def post_to_slack(temp):
    url = 'https://hooks.slack.com/services/{YOUR_INCOMING_WEBHOOK}'
    payload =  { 'text': 'Roberto\'s office temperature is {:.2f} F'.format(temp) }
    requests.post(url, json=payload)

hat = SenseHat()
while True:
    tempC = hat.get_temperature()
    tempF = convert_to_F(tempC)
    print('[{}] Temperature {:.2f}C / {:.2f}F'.format(datetime.utcnow(), tempC , tempF))

    # post_to_slack(tempF)
    display_on_lcd(tempF)
    post_to_azure(tempF, hat.get_humidity())

    # (8*5) second delay.
    display_progress_bar()

