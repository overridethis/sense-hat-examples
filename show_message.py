from sense_hat import SenseHat
import sys

hat = SenseHat()
msg = sys.argv[1]

for i in range(0,3):
    hat.show_message(msg, 0.05, (255,0,0))

hat.clear()
